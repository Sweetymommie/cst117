﻿// Shiffon Richardson, CST-117 (accommodated schedule)
// I learned this code at Starting out with Visual C#, Fourth Edition, by Tony Gaddis. Published by Pearson. Copyright © 2017 by Pearson Education, Inc.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp16
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }


        private void displayButton_Click_1(object sender, EventArgs e)
        {
            StyleForm styleForm = new StyleForm();

            // Find the selected radio button.
            styleForm.Show();

            if (sandalsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "silver";
                styleForm.colorLabel.Text = "navy";
                styleForm.heelLabel.Text = "1 inch wedge";
            }
            if (pumpsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "color matched";
                styleForm.colorLabel.Text = "emerald";
                styleForm.heelLabel.Text = "2 inch block";
            }
            if (flatsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "gold";
                styleForm.colorLabel.Text = "great sun";
                styleForm.heelLabel.Text = "1/8 inch";
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            StyleForm styleForm = new StyleForm();

            // Find the selected radio button.
            styleForm.Show();

            if (sandalsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "silver";
                styleForm.colorLabel.Text = "navy";
                styleForm.heelLabel.Text = "1 inch wedge";
            }
            if (pumpsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "color matched";
                styleForm.colorLabel.Text = "emerald";
                styleForm.heelLabel.Text = "2 inch block";
            }
            if (flatsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "gold";
                styleForm.colorLabel.Text = "great sun";
                styleForm.heelLabel.Text = "1/8 inch";
            }


            //The following code displays "7"
            /*string str = "great sun";
            int position = str.LastIndexOf('g', 7, 2);
            if (position != -1)
            {
                MessageBox.Show(position.ToString());
            }
            else
            {
                MessageBox.Show("e was not found.");
            }*/
        } // end button

        private void restockButton_Click_1(object sender, EventArgs e)
        {
            StyleForm styleForm = new StyleForm();

            // Find the selected radio button.
            styleForm.Show();

            if (sandalsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "silver";
                styleForm.colorLabel.Text = "navy";
                styleForm.heelLabel.Text = "1 inch wedge";
            }
            if (pumpsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "color matched";
                styleForm.colorLabel.Text = "emerald";
                styleForm.heelLabel.Text = "2 inch block";
            }
            if (flatsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "gold";
                styleForm.colorLabel.Text = "great sun";
                styleForm.heelLabel.Text = "1/8 inch";
            }
        }
        private void removeButton_Click_1(object sender, EventArgs e)
        {
            StyleForm styleForm = new StyleForm();

            // Find the selected radio button.
            styleForm.Show();

            if (sandalsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "silver";
                styleForm.colorLabel.Text = "navy";
                styleForm.heelLabel.Text = "1 inch wedge";
            }
            if (pumpsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "color matched";
                styleForm.colorLabel.Text = "emerald";
                styleForm.heelLabel.Text = "2 inch block";
            }
            if (flatsRadioButton.Checked == true)
            {
                styleForm.buckleLabel.Text = "gold";
                styleForm.colorLabel.Text = "great sun";
                styleForm.heelLabel.Text = "1/8 inch";
            }
        }
        private void addButton_Click_1(object sender, EventArgs e)
        {
            
            if (sandalsRadioButton.Checked == true)
            {
                listBox1.Items.Add(textBox1.Text);
            }
            if (pumpsRadioButton.Checked == true)
            {
                listBox2.Items.Add(textBox1.Text);
            }
            if (flatsRadioButton.Checked == true)
            {
                listBox3.Items.Add(textBox1.Text);
            }
            
        }

       
    }
}
                            
                        
           