﻿namespace WindowsFormsApp13
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private system.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.CharacterCasing = system.Windows.Forms.CharacterCasing.Lower;
            this.textBox1.HideSelection = false;
            this.textBox1.Location = new system.Drawing.Point(296, 74);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new system.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new system.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.AcceptsReturn = true;
            this.textBox2.CharacterCasing = system.Windows.Forms.CharacterCasing.Upper;
            this.textBox2.HideSelection = false;
            this.textBox2.Location = new system.Drawing.Point(296, 117);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new system.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new system.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = system.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new system.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Programming Project 3";
            this.Load += new system.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private system.Windows.Forms.OpenFileDialog openFileDialog1;
        private system.Windows.Forms.TextBox textBox1;
        private system.Windows.Forms.TextBox textBox2;
    }
}

