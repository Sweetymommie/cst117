﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calories_fat_and_carbs
{
    public partial class Form1 : Form
{
    public Form1()
    {
        InitializeComponent{ };
    }

    // The Methods Used
    private double fatCalories(double fatGrams)
    {
        return fatGrams * 9;
    }
    private double carbsCalories(double carbGrams)
        {
            return carbGrams * 4;
        }

        private void convertButton_Click(object sender, EventArgs e)

            // Variables for Fat, Carbs and Calories
            double calFromFat, fatGrams, calsFromCarbs, carbsGrams;
            
            // Get the Number of fatGrams
            if (double.TryParse(fatTextBox.Text, out fatGrams))
    {            
        if (double.TryParse(fatTextBox.Text, out fatGrams))
    {
        // Calculate Calories From Fat
        calFromFat = fatGrams* 9;

        // Display Fat Calories
        calFatLabel.Text = calFromFat.ToString("n1");

        }
        else
        {
        // Display Error Message
        MessageBox.Show("Enter a valid number for fatGrams.");
        }

        // Get the Number of Carbs Grams
        if (double.TryParse(carbsTextBox.Text, out carbsGrams))
        {

        // Calculate Calories from Carbs
        calFromCarbs = carbsGrams * 4;

        // Display Carbs to Calories
        calCarbsLabel.Text = calFromCarbs.ToString("n1");

        }
        else
        {

        //Display Error Message
        MessageBox.Show("Enter a valid number for carbs Grams.");
        }
    }

        private void clearButton_Click(object sender, EventArgs e)

{
    {
        // Clear the Input and Output Controls
        fatTextBox.Text = "";
        carbsTextBox.Text = "";
        calFatLabel1.Text = "";
        calCarbsLabel1.Text = "";
        fatTextBox.Focus();

    }

    private void exitButton_Click(object sender, EventArgs e)

    {
        // Close the Form
        this.Close();
    }
          
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
[STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
