﻿namespace WindowsFormsApp16
{
    partial class StyleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buckleLabel = new System.Windows.Forms.Label();
            this.colorLabel = new System.Windows.Forms.Label();
            this.heelLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buckleLabel
            // 
            this.buckleLabel.AutoSize = true;
            this.buckleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buckleLabel.Location = new System.Drawing.Point(67, 61);
            this.buckleLabel.Name = "buckleLabel";
            this.buckleLabel.Size = new System.Drawing.Size(55, 16);
            this.buckleLabel.TabIndex = 3;
            this.buckleLabel.Text = "Buckle";
            // 
            // colorLabel
            // 
            this.colorLabel.AutoSize = true;
            this.colorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colorLabel.Location = new System.Drawing.Point(67, 111);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(45, 16);
            this.colorLabel.TabIndex = 4;
            this.colorLabel.Text = "Color";
            // 
            // heelLabel
            // 
            this.heelLabel.AutoSize = true;
            this.heelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heelLabel.Location = new System.Drawing.Point(67, 166);
            this.heelLabel.Name = "heelLabel";
            this.heelLabel.Size = new System.Drawing.Size(41, 16);
            this.heelLabel.TabIndex = 5;
            this.heelLabel.Text = "Heel";
            // 
            // StyleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 308);
            this.Controls.Add(this.heelLabel);
            this.Controls.Add(this.colorLabel);
            this.Controls.Add(this.buckleLabel);
            this.Name = "StyleForm";
            this.Text = "Result";
            this.Load += new System.EventHandler(this.StyleForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label buckleLabel;
        public System.Windows.Forms.Label colorLabel;
        public System.Windows.Forms.Label heelLabel;
    }
}