﻿// I learned this code at https://codereview.stackexchange.com/questions/172042/simple-c-console-tic-tac-toe-program; http://visualstudioapplications.blogspot.com/2012/09/creating-tic-tac-toe-with-c-part-2.html;
// and Starting out with Visual C#, Fourth Edition, by Tony Gaddis. Published by Pearson. Copyright © 2017 by Pearson Education, Inc.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp15
{
    public partial class tictactoe1 : tictactoe1
    {
        public tictactoe1() => InitializeComponent();

        private void label1_Click(object sender, EventArgs e)
        {
            bool[,] X, O;
            string player;
            bool finished; // true when the game finish
            int difficulity;
            string ButtonsIndexs; //Indexs of remaining buttons
            int x, y; //coordinate of button
            Random rnd;
        }
        const int ROWS = 3;
        const int COLS = 3;
        int[,] numbers = { {1, 2, 3},
                           {4, 5, 6},
                           {7, 8, 9}
};
        {
public int[,] Numbers { get => Numbers1; set => Numbers1 = value; }
        public int[,] Numbers1 { get => numbers; set => numbers = value; }
        public int[,] Numbers2 { get => numbers; set => numbers = value; }

        private void resetVars()
        {
            ButtonsIndexs = "123456789";
            finished = false;
            rnd = new Random();
            difficulity = 0;
            O = new bool[3, 3];
            X = new bool[3, 3];
        }

        {
        private void write(string p, Button targetButton)
        {
            if (p == "O")
            {
                targetButton.ForeColor = Color.Red;
                O[x, y] = true; //the place (x,y) is occupied by an 'O'
            }
            else
            {
                targetButton.ForeColor = Color.Blue;
                X[x, y] = true; //the place (x,y) is occupied by an 'X'
            }
            targetButton.Text = p; //Writing X or O

            //Removing the index of the clicked button from ButtonIndexs
            if (targetButton == button1)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('1'), 1);
            if (targetButton == button2)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('2'), 1);
            if (targetButton == button3)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('3'), 1);
            if (targetButton == button4)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('4'), 1);
            if (targetButton == button5)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('5'), 1);
            if (targetButton == button6)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('6'), 1);
            if (targetButton == button7)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('7'), 1);
            if (targetButton == button8)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('8'), 1);
            if (targetButton == button9)
                ButtonsIndexs = ButtonsIndexs.Remove(ButtonsIndexs.IndexOf('9'), 1);
        }
        private void EasyPCWrite(string p)
        {
            // a Random button index (to chose a random free button)
            int index = int.Parse(ButtonsIndexs[rnd.Next(ButtonsIndexs.Length)].ToString());
            switch (index)
            {
                case 1: x = y = 0; write(p, button1); break;
                case 2: x = 0; y = 1; write(p, button2); break;
                case 3: x = 0; y = 2; write(p, button3); break;
                case 4: x = 1; y = 0; write(p, button4); break;
                case 5: x = 1; y = 1; write(p, button5); break;
                case 6: x = 1; y = 2; write(p, button6); break;
                case 7: x = 2; y = 0; write(p, button7); break;
                case 8: x = 2; y = 1; write(p, button8); break;
                case 9: x = 2; y = 2; write(p, button9); break;
            }

            {
                private void NormalPCWrite(string p)
                }
                private bool Diagonals(bool[,] current)
                {
                    return (
                        (current[0, 0] && current[1, 1] && current[2, 2])
                        ||
                        (current[0, 2] && current[1, 1] && current[2, 0])
                        );
                }

                {
                    private bool Row(int p, bool[,] current)
                    {
                        return (current[p, 0] && current[p, 1] && current[p, 2]);
                    }

                private bool Column(int p, bool[,] current)
                {
                    return (current[0, p] && current[1, p] && current[2, p]);
                }
                // checking for win or draw
                private bool CheckForWinOrDraw(bool[,] current)
                {
                    if (Row(0, current) || Row(1, current) || Row(2, current) || Column(0, current) || Column(1, current) || Column(2, current) || Diagonals(current))
                    {
                        //updating score
                        if (current == X)
                            xLabel.Text = (int.Parse(xLabel.Text) + 1).ToString();
                        else
                            oLabel.Text = (int.Parse(oLabel.Text) + 1).ToString();
                        //game finished
                        finished = true;
                        MessageBox.Show(string.Format("{0} Wins", (ButtonsIndexs.Length % 2 == 0) ? "X" : "O"));//showing result
                    }
                    else
                    {
                        if (ButtonsIndexs == "")
                        {
                            //game finished
                            finished = true;
                            MessageBox.Show("Draw");
                        }

                    }
                    return finished;
                }
                private void mainButtons(object sender, EventArgs e)
                {
                    player = (sender as Button).Text; //user choice, X or O
                    this.Size = new Size(382, 300); //changes form size
                    tableLayoutPanel1.Visible = true;
                    if (difficulity == 0)
                        if (player == "O")
                            NewMethod1();
                        else
                        {
                            return;
                        }
                    else
                        if (player == "O")
                        NewMethod();
                    {
                        private void backButton_Click(object sender, EventArgs e)
                        {
                            Form1 f1 = new Form1();
                            this.Hide();
                            f1.Show();
                        }
                        {
                            private void Form3_Load(object sender, EventArgs e)
                            {
                                resetVars();
                            }
                            {
                                private void playerVsPC_Click(object sender, EventArgs e)
                                {
                                    Form3 f3 = new Form3();
                                    this.Hide();
                                    f3.Show();
                                }
                            }
                        }

                    }
                }
                {

                }

            }
        }

        {
private static void NewMethod1()
        {
            player.Write("X");
        }

        private void NewMethod()
        {
            Write("X");
        }

        private void Write(string v)
        {
            throw new NotImplementedException();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
