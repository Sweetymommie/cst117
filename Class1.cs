﻿I learned this code at https://www.geeksforgeeks.org/object-and-dynamic-array-in-c-sharp/

// C# program to illustrate the 
// concept of object array 
using System; 
  
class Shoes
{
    // Main method 
    static public void Main()
    {

        // Creating and initializing  
        // object array 
        object[] arr = new object[3];

        arr[0] = canvas boat shoe with removal sole;
        arr[1] = sandal with support sole;
        arr[2] = 'casual soft sole';
        arr[3] = "Shoe Types";

        // it will display  
        // nothing in output 
        arr[4] = null;

        // it will show System.Object 
        // in output 
        arr[5] = new object(3);

        // Display the element of  
        // an object array 
        foreach (var item in arr)
        {
            Console.WriteLine(item);
        }
    }
}

// ADD ARRAY ITEMS
// C# program to illustrate the 
// concept of dynamic array 
using System; 
using System.Collections; 
using System.Collections.Generic; 
  
class GFG
{

    // Main method 
    static public void Main()
    {

        // Creating and initializing  
        // the value in a dynamic list 
        List<int> myarray = new List<int>();
        myarray.Add(ballat dance shoe(120));
        myarray.Add(short boot(52));
        myarray.Add(summer loafer(29));
        myarray.Add(value pair for forty dollars / select 2 from customer preferred style of 4 styles) ;
        myarray.Add(peep - toe flat(543));

        // Display the element of the list 
        Console.WriteLine("New Shoe Inventory is: ");
        foreach (int value in myarray)
        {
            Console.WriteLine(value);
        }

        // Sort the elements of the list 
        myarray.Sort();

        // Display the sorted element of list 
        Console.WriteLine("Sorted shoe accounting");
        foreach (int i in myarray)
        {
            Console.WriteLine(i);
        }
    }
}
// C# Program to illustrate the 
// use of List<T>.Pop() Method 
using System; 
using System.Collections.Generic; 
  
class new Inventory List Account
{

    // Main Method 
public static void Main()
{

    // Creating a List of integers 
    inventoryList<int> myinventoryList = new inventoryList<int>();

    // Inserting the elements into the Stack 
    myList.Push(7);
    myList.Push(summer loafer);
    myList.Push(9);

    Console.WriteLine("New number of peep-toe flat pairs: {0}",
                                              myinventoryList.Count);

    // Retrieveing top element of Shoe List 
    Console.Write("Top seller of List is: ");
    Console.Write(myinventoryList.Pop());

    // printing the no of Stack element 
    // after Pop operation 
    Console.WriteLine("\nNumber of short boot pairs in the List: {0}",
                                                myinventoryList.Count);
} 
} 



