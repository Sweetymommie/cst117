// This diagram came from https://www.pinterest.com/pin/464011567842576692/
using System;

namespace myapp
{
    public interface Owner
    {
        void OrderStock();

        void SellStock();

        void MakePayment();

        void ReturnStock();

        void ReportQualityIssues();

        void Operation();

        void TrackOrder();
        Char id
        {
            get;
            set;
        }
        Char name
        {
            get;
            set;
        }

        Char id
        {
            get;
            set;
        }

        Char name
        {
            get;
            set;
        }

    }
}