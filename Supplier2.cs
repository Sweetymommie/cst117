using System;

namespace myapp
{
    public interface Supplier2
    {
        void ReceivePayment();

        void SupplyStock();

        void TrackOrders();

        Char id
        {
            get;
            set;
        }

        Char name
        {
            get;
            set;
        }

        int PhNo
        {
            get;
            set;
        }

        String address
        {
            get;
            set;
        }

    }
}