﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// I learned some of this Code from https://www.ict.social/csharp/oop/rolling-die-in-csharp-net-constructors-and-random-numbers
// I learned from Thinking Storm

namespace ConsoleApp2
{
    class RollingDie
    {
        private Random random;
        private int sidesCount;

        public RollingDie()
        {
            sidesCount = 6;
            random = new Random();
        }

        public RollingDie(int sidesCount)
        {
            this.sidesCount = sidesCount;
            random = new Random();
        }

        // Returns number of sides the die has
        public int GetSidesCount()
        {
            return sidesCount;
        }

        public int Roll()
        {
            return random.Next(1, sidesCount + 1);
        }

        public override string ToString()
        {
            return String.Format("Rolling a die with {0} sides", sidesCount);
        }


        class Program
        {
            static void Main(string[] args)
            {
                
                bool isSnakeEyes = false;
               // int counter;

            // Create instances
            RollingDie sixSided = new RollingDie();
                RollingDie tenSided = new RollingDie(10);

                // Rolls the 6-sided die
                Console.WriteLine(sixSided);
                for (int i = 0; i < 10; i++)
                    Console.Write(sixSided.Roll() + " ");

                // Rolls the 10-sided die
                Console.WriteLine("\n\n" + tenSided);
                for (int i = 0; i < 10; i++)
                    Console.Write(tenSided.Roll() + " ");

                Console.ReadKey();

                for (int i = 0; i < 2; i++)
                {
                    isSnakeEyes = true
                    Console.WriteLine("\n\n" + tenSided);
                   
                        Console.Write(tenSided.Roll() + "It took you {} rolls to get to snake eyes!");
                }
            }
        }
    }
}

