﻿using System;

namespace WindowsFormsApp16
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.sandalsRadioButton = new System.Windows.Forms.RadioButton();
            this.pumpsRadioButton = new System.Windows.Forms.RadioButton();
            this.flatsRadioButton = new System.Windows.Forms.RadioButton();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.displayButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.restockButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "silver",
            "navy",
            "1 inch wedge"});
            this.listBox1.Location = new System.Drawing.Point(437, 63);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(111, 43);
            this.listBox1.TabIndex = 0;
            // 
            // sandalsRadioButton
            // 
            this.sandalsRadioButton.AutoSize = true;
            this.sandalsRadioButton.Checked = true;
            this.sandalsRadioButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sandalsRadioButton.Location = new System.Drawing.Point(25, 63);
            this.sandalsRadioButton.Name = "sandalsRadioButton";
            this.sandalsRadioButton.Size = new System.Drawing.Size(84, 20);
            this.sandalsRadioButton.TabIndex = 4;
            this.sandalsRadioButton.TabStop = true;
            this.sandalsRadioButton.Text = "Sandals";
            this.sandalsRadioButton.UseVisualStyleBackColor = true;
            // 
            // pumpsRadioButton
            // 
            this.pumpsRadioButton.AutoSize = true;
            this.pumpsRadioButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpsRadioButton.Location = new System.Drawing.Point(25, 134);
            this.pumpsRadioButton.Name = "pumpsRadioButton";
            this.pumpsRadioButton.Size = new System.Drawing.Size(73, 20);
            this.pumpsRadioButton.TabIndex = 5;
            this.pumpsRadioButton.TabStop = true;
            this.pumpsRadioButton.Text = "Pumps";
            this.pumpsRadioButton.UseVisualStyleBackColor = true;
            // 
            // flatsRadioButton
            // 
            this.flatsRadioButton.AutoSize = true;
            this.flatsRadioButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatsRadioButton.Location = new System.Drawing.Point(25, 220);
            this.flatsRadioButton.Name = "flatsRadioButton";
            this.flatsRadioButton.Size = new System.Drawing.Size(63, 20);
            this.flatsRadioButton.TabIndex = 6;
            this.flatsRadioButton.TabStop = true;
            this.flatsRadioButton.Text = "Flats";
            this.flatsRadioButton.UseVisualStyleBackColor = true;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Items.AddRange(new object[] {
            "color matched",
            "emerald",
            "2 inch block"});
            this.listBox2.Location = new System.Drawing.Point(437, 134);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(111, 43);
            this.listBox2.TabIndex = 7;
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Items.AddRange(new object[] {
            "gold",
            "great sun",
            "1/8 inch"});
            this.listBox3.Location = new System.Drawing.Point(437, 220);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(111, 43);
            this.listBox3.TabIndex = 8;
            // 
            // displayButton
            // 
            this.displayButton.Location = new System.Drawing.Point(595, 75);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(75, 23);
            this.displayButton.TabIndex = 9;
            this.displayButton.Text = "Display";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click_1);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(595, 145);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 10;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // restockButton
            // 
            this.restockButton.Location = new System.Drawing.Point(595, 220);
            this.restockButton.Name = "restockButton";
            this.restockButton.Size = new System.Drawing.Size(75, 23);
            this.restockButton.TabIndex = 11;
            this.restockButton.Text = "Restock";
            this.restockButton.UseVisualStyleBackColor = true;
            this.restockButton.Click += new System.EventHandler(this.restockButton_Click_1);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(595, 274);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 12;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click_1);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(595, 319);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 13;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(437, 319);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.restockButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.flatsRadioButton);
            this.Controls.Add(this.pumpsRadioButton);
            this.Controls.Add(this.sandalsRadioButton);
            this.Controls.Add(this.listBox1);
            this.Name = "MainForm";
            this.Text = "Shoes Inventory List";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.RadioButton sandalsRadioButton;
        private System.Windows.Forms.RadioButton pumpsRadioButton;
        private System.Windows.Forms.RadioButton flatsRadioButton;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Button displayButton;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button restockButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox textBox1;
    }
}

