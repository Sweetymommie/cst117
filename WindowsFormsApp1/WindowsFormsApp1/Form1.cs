﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Color_Theme
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

#pragma warning disable IDE1006 // Naming Styles
        private void Form1_Load(object sender, EventArgs e)
#pragma warning restore IDE1006 // Naming Styles
        {

        }

#pragma warning disable IDE1006 // Naming Styles
        private void yellowradioButton_CheckedChanged(object sender, EventArgs e)
#pragma warning restore IDE1006 // Naming Styles
        {
            object yellowradioButton_CheckedChanged = yellowradioButton_CheckedChanged;
            object yellowRadioButton = yellowRadioButton5;
            if (yellowradioButton_CheckedChanged.Checked)
            {
                this.BackColor = Color.Yellow;
            }
        }

#pragma warning disable IDE1006 // Naming Styles
        private void redRadioButton_CheckedChanged(object sender, EventArgs e)
#pragma warning restore IDE1006 // Naming Styles
        {
            if  (redRadioButton.Checked)
            {
                this.BackColor = Color.Red;
            }
        }

#pragma warning disable IDE1006 // Naming Styles
         private void whiteRadioButton_CheckedChanged(object sender, EventArgs e)
#pragma warning restore IDE1006 // Naming Styles
        {
            if (whiteRadioButton.Checked)
            {
                this.BackColor = Color.White;
            }
        }

#pragma warning disable IDE1006 // Naming Styles
        private void normalRadioButton_CheckedChanged(object sender, EventArgs e)
#pragma warning restore IDE1006 // Naming Styles
        {
            if  (normalRadioButton.Checked)
            {
                this.BackColor = SystemColors.Control;
            }
        }

#pragma warning disable IDE1006 // Naming Styles
        private void exitButton_CheckedChanged(object sender, EventArgs e)
#pragma warning restore IDE1006 // Naming Styles
        {
            // Close the form.
            this.Close();
        }
    }
}
